<?php
include "config/setup.php";
if (empty($_SESSION["logged_on_user"])) {
	header("Location: /index.php");
	die();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Camagru</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body onload="initNotificationForm()">
		<h1><a href="/index.php">camagru</a></h1>

		<div id="menu-container">
		<ul>
			<li><a href="/modify_account.php"><strong><?php echo($_SESSION["logged_on_user"]) ?></strong></a></li>
			<li><a href='/backend/logout.php'>Se deconnecter</a></li>
			<li><a href='/picture.php'>Allons prendre des p'tites photos!</a></li>
			<li><a href='/gallery.php'>C'est partis pour la gallerie</a></li>
		</ul>
		</div>

		<h2>Modifier le compte</h2>
		<?php
			if ($_GET["error"] == "email_exists")
				echo("<p class='notify-bad'>Cette adresse mail est deja utilise.</p>");
			else if ($_GET["error"] == "invalid_email")
				echo("<p class='notify-bad'>L'adresse email renseigne est invalide.</p>");
			else if ($_GET["error"] == "success")
				echo("<p class='notify-good'>Vos informations ont bien ete modifie.</p>");
			else if ($_GET["error"] == "login_already_exists")
				echo("<p class='notify-bad'>Cet identifiant n'est pas disponible.</p>");
		?>

		<form action="/backend/change_info.php" method="post" class='myform'>
			Nouvel identifiant:
			<br />
			<input type="text" name="login">
			<input type="submit" name="submit" value="OK">
		</form>
		<br />
		<form action="/backend/change_info.php" method="post" class='myform'>
			Nouveau mot de passe: <input type="password" name="passwd">
			<input type="submit" name="submit" value="OK">
		</form>
		<br />
		<form action="/backend/change_info.php" method="post" class='myform'>
			Nouvel adresse email: <input type="text" name="email">
			<input type="submit" name="submit" value="OK">
		</form>
		<br />
		<form action="/backend/change_info.php" method="post" class='myform' id="notif-form">
			Activer les notifications:
			<input type="radio" name="notification" value="yes">Oui</input>
			<input type="radio" name="notification" value="no">Non</input>
			<input type="submit" name="submit" value="OK">
		</form>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

<script>
	function initNotificationForm(ev) {
		var form = document.getElementById("notif-form");

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/get_notify.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(eve) {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				if (xhr.response == "0")
					form["1"].checked = true;
				else if (xhr.response == "1")
					form["0"].checked = true;
			}
		}
		xhr.send();
	}

	function likePic(ev) {
		var id = ev.parentNode.children["id"].getAttribute("value");

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/like_pic.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(eve) {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				xhr_decoded = JSON.parse(xhr.response);
				ev.parentNode.children["likes_count"].innerHTML = xhr_decoded["likes"];
				if (xhr_decoded["selected"])
					ev.parentNode.children["thumb"].classList.add("selected");
				else
					ev.parentNode.children["thumb"].classList.remove("selected");
			}
		}
		xhr.send("id=" + encodeURIComponent(id));
	}
</script>

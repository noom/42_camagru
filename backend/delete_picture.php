<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"])) {
	echo("error-user");
}

if (empty($_POST["id"])) {
	echo("error-pic");
	die();
}

pic_delete($_SESSION["logged_on_user"], $_POST["id"]);
?>

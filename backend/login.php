<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_POST["login"]) || empty($_POST["passwd"])) {
	header('Location: /login_page.php?log=empty');
}
else if (user_auth(htmlspecialchars($_POST["login"]), htmlspecialchars($_POST["passwd"])) == true) {
	$_SESSION["logged_on_user"] = htmlspecialchars($_POST["login"]);
	header('Location: /index.php?log=in');
}
else {
	unset($_SESSION["logged_on_user"]);
	header('Location: /login_page.php?log=wrong');
}
die();
?>

<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (!empty($_POST["login"])) {
	if (user_exists($_POST["login"])) {
		header("Location: /modify_account.php?error=login_already_exists");
		die();
	}
	user_change_login($_SESSION["logged_on_user"], $_POST["login"]);
}

else if (!empty($_POST["passwd"])) {
	user_change_passwd($_SESSION["logged_on_user"], $_POST["passwd"]);
}

else if (!empty($_POST["email"])) {
	if (email_exists($_POST["email"])) {
		header("Location: /modify_account.php?error=email_exists");
		die();
	}
	if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {
		header("Location: /modify_account.php?error=invalid_email");
		die();
	}
	user_change_email($_SESSION["logged_on_user"], $_POST["email"]);
}

else if (!empty($_POST["notification"])) {
	if ($_POST["notification"] == "yes")
		user_change_notify($_SESSION["logged_on_user"], "1");
	else if ($_POST["notification"] == "no")
		user_change_notify($_SESSION["logged_on_user"], "0");
}

header("Location: /modify_account.php?error=success");
die();
?>

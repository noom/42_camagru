<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"]) || empty($_GET["comment_input"])) {
	header('Location: /gallery.php?page='. $_GET["page"]);
	die();
}

$_GET["comments_input"] = htmlspecialchars($_GET["comments_input"]);

comments_idpic_add($_GET["id_pic"], $_SESSION["logged_on_user"], $_GET["comment_input"]);

$login_of_picauthor = pic_login_of($_GET["id_pic"]);

if ($login_of_picauthor != $_SESSION["logged_on_user"]
	&& notification_enabled_for($login_of_picauthor)) {
	notify_pic_author($_GET["id_pic"], $_SESSION["logged_on_user"], $_GET["comment_input"]);
}

header('Location: /gallery.php?page='. $_GET["page"]);
/* header('Location: /gallery.php'); */
die();
?>

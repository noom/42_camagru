<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

$pics = pic_all();

$pics_total = count($pics);
$pic_per_page = 10;
$pages_total = ceil($pics_total / $pic_per_page);

if (empty(($current_page = $_GET["page"])))
	$current_page = 1;

if ($current_page < 1)
	$current_page = 1;
else if ($current_page > $pages_total)
	$current_page = $pages_total;

$start = ($current_page - 1) * $pic_per_page;
$end = $start + $pic_per_page;

echo("<div id='menu'>");
if ($current_page > 1)
	echo("<a href='/gallery.php?page=". ($current_page - 1) ."'><</a>");

for ($i = 1; $i < $pages_total + 1; $i++) {
	if ($i != $current_page)
		echo("<a href='/gallery.php?page=$i'>$i</a>");
	else
		echo("<span class='menu_selected'>$i</span>");
}

if ($current_page < $pages_total)
	echo("<a href='/gallery.php?page=". ($current_page + 1) ."'>></a>");
echo("</div>");

echo("<div id='gallery-container'>");
$i = -1;
foreach ($pics as $pic) {
	$i++;
	if ($i < $start)
		continue;
	if ($i > $end - 1)
		break;

	$likes = pic_likes_of($pic["id"]);
	$likes_count = count($likes);
	$thumb_selected = "";

	if (like_query($_SESSION["logged_on_user"], $pic["id"])) {
		$thumb_selected = "selected";
	}

	echo("<div class='pic-holder'>".
			"<img class='pic' src='data:image/png;base64,". $pic["pic_b64"] ."'>".
			"<div class='comments-holder'>");

	$comments = array_reverse(comments_idpic($pic["id"]));
	foreach ($comments as $comment) {
		$comment["login"] = user_login_byid($comment["id_login"]);
		echo("<div class='comment'>");

		if ($comment["login"] == $_SESSION["logged_on_user"]) {
			echo("<a href='/backend/comment_remove.php?id=". $comment["id"] ."'>".
					"<img class='delete-cross' src='/img/delete.png'></img>".
				"</a>");
		}

		echo("	<p>". $comment["login"] .": ". $comment["text"] ."<p>".
			"</div><hr/>");
	}

	echo("	</div>".
			"<div class='input-line'>");

	if (!empty($_SESSION["logged_on_user"])) {
		echo(	"<form action='/backend/comment.php' method='get' id='comment-form'>".
					"<input type='hidden' name='page' value='$current_page'>".
					"<input type='hidden' name='id_pic' value='". $pic["id"] ."'>".
					"<input type='text' name='comment_input'>".
					"<input type='submit' name='submit' value='Commenter'>".
				"</form>");
	}

	echo(		"<div class='thumb-count'>".
					"<input type='hidden' name='id' value='". $pic["id"] ."'>".
					"<img name='thumb' class='thumb $thumb_selected' src='/img/thumb_up.png'");

	if (!empty($_SESSION["logged_on_user"]))
		echo ("onclick='likePic(this)'");

	echo(			"><div name='likes_count'>$likes_count</div>".
				"</div>".
			"</div>".
		"</div>");
}
echo("</div>");
?>

<?php
include $_SERVER["DOCUMENT_ROOT"] ."/config/setup.php";

function email_exists($email) {
	global $db;
	$query = $db->prepare("SELECT `email` FROM `users`
		WHERE `email` = :email");
	$query->execute([":email" => $email]);
	return count($query->fetch()) > 1;
}

function user_exists($login) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `users`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	return count($query->fetch()) > 1;
}

function user_add($login, $passwd, $email) {
	$passwd_hash = hash("whirlpool", $passwd);
	$email_code = hash("md5", $passwd_hash);
	global $db;
	$query = $db->prepare("INSERT INTO `users`
		VALUES(0, :login, :passwd, :email, :email_code, 1)");
	$query->execute([
		":login" => $login,
		":passwd" => $passwd_hash,
		":email" => $email,
		":email_code" => $email_code]);

	$to      = $email;
	$subject = "Camagru - Confirmes ton compte!";
	$message = "Coucou, cliques sur le lien ci-dessous please!\r\n".
		"http://". $_SERVER["HTTP_HOST"] ."/backend/confirm_account.php?email_code=".
		urlencode($email_code) ."&login=$login\r\n";
	$headers = "From: echojnow@student.42.fr\r\n";

	mail($to, $subject, $message, $headers);
}

function user_id_bylogin($login) {
	global $db;
	$query = $db->prepare("SELECT `id` FROM `users`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	return $query->fetch()[0];
}

function user_login_byid($id) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `users`
		WHERE `id` = :id");
	$query->execute([":id" => $id]);
	return $query->fetch()[0];
}

function user_change_login($login, $new_login) {
	global $db;
	$query = $db->prepare("UPDATE `users` SET login = :new_login
		WHERE `login` = :login");
	$query->execute([
		":new_login" => $new_login,
		":login" => $login]);
	$_SESSION["logged_on_user"] = $new_login;
}

function user_change_passwd($login, $new_passwd) {
	global $db;
	$query = $db->prepare("UPDATE `users` SET password = :new_passwd
		WHERE `login` = :login");
	$query->execute([
		":new_passwd" => hash("whirlpool", $new_passwd),
		":login" => $login]);
}

function user_change_email($login, $new_email) {
	global $db;
	$query = $db->prepare("UPDATE `users` SET email = :new_email
		WHERE `login` = :login");
	$query->execute([
		":new_email" => $new_email,
		":login" => $login]);
}

function user_forgot_passwd($login) {
	global $db;
	$query = $db->prepare("SELECT `email` FROM `users`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	$email = $query->fetch()[0];

	$new_passwd = substr(hash("whirlpool", rand(1, 1000000) . $login), 0, rand(7, 9));

	user_change_passwd($login, $new_passwd);

	$to      = $email;
	$subject = "Camagru - Mot de passe oublie";
	$message = "Voici ton nouveau mot de passe: $new_passwd\r\n".
		"http://". $_SERVER["HTTP_HOST"] ."/login_page.php\r\n";
	$headers = "From: echojnow@student.42.fr\r\n";

	mail($to, $subject, $message, $headers);
}

function user_auth($login, $passwd) {
	global $db;
	$query = $db->prepare("SELECT `login`, `password`, `email_code` FROM `users`
		WHERE `login` = :login AND `password` = :passwd");
	$query->execute([
		":login" => $login,
		":passwd" => hash("whirlpool", $passwd)]);
	$row = $query->fetch();
	return (!empty($row["password"]) && $row["email_code"] == 1);
}

function user_validate($login, $email_code) {
	$login = htmlspecialchars($login);
	global $db;
	$query = $db->prepare("SELECT `login`, `email_code` FROM `users`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	$row = $query->fetch();
	if ($row["email_code"] != $email_code) {
		header("Location: /login_page.php?log=email_ko");
		die();
	}
	$db->exec("UPDATE `users` SET email_code = '1' WHERE `login` = '$login'");
	header("Location: /login_page.php?log=email_ok");
	die();
}

function user_get_email($login) {
	global $db;
	$query = $db->prepare("SELECT `email` FROM `users`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	return $query->fetch();
}

function pic_save($login, $pic) {
	global $db;
	$query = $db->prepare("INSERT INTO `pictures`
		VALUES(0, :login, :pic)");
	$query->execute([
		":login" => $login,
		":pic" => base64_encode($pic)]);
	return $db->lastInsertId();
}

function pic_delete($login, $id) {
	global $db;
	$query = $db->prepare("DELETE FROM `pictures`
		WHERE `login` = :login AND `id` = :id;");
	$query->execute([
		":login" => $login,
		"id" => $id]);

	$query = $db->prepare("DELETE FROM `likes`
		WHERE `id_pic` = :id;");
	$query->execute(["id" => $id]);

	$query = $db->prepare("DELETE FROM `comments`
		WHERE `id_pic` = :id;");
	$query->execute(["id" => $id]);
}

function pic_by($login) {
	global $db;
	$query = $db->prepare("SELECT `id`, `pic_b64` FROM `pictures`
		WHERE `login` = :login");
	$query->execute([":login" => $login]);
	return $query->fetchAll();
}

function pic_all() {
	global $db;
	$query = $db->prepare("SELECT `id`, `login`, `pic_b64` FROM `pictures` order by id desc;");
	/* $query = $db->prepare("SELECT `id`, `login`, `pic_b64` FROM `pictures`;"); */
	$query->execute();
	return $query->fetchAll();
}

function pic_likes_of($id_pic) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `likes`
		WHERE `id_pic` = :id_pic");
	$query->execute([":id_pic" => $id_pic]);
	return $query->fetchAll();
	/* file_put_contents("TEST", print_r($query->fetchAll(), 1)); */
	/* return $query->rowCount(); */
}

function pic_login_of($id_pic) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `pictures`
		WHERE `id` = :id_pic");
	$query->execute([":id_pic" => $id_pic]);
	return $query->fetch()[0];
}

function like_query($login, $id_pic) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `likes`
		WHERE `login` = :login AND `id_pic` = :id_pic");
	$query->execute([
		":login" => $login,
		":id_pic" => $id_pic]);
	$fetch = $query->fetchAll();
	return count($fetch) > 0 ? true : false;
}

function like_pic_id($login, $id_pic) {
	global $db;
	if (like_query($login, $id_pic)) {
		$query = $db->prepare("DELETE FROM `likes`
			WHERE `id_pic` = :id_pic AND `login` = :login");
	}
	else {
		$query = $db->prepare("INSERT INTO `likes` VALUES(0, :id_pic, :login)");
	}
	$query->execute([
		":id_pic" => $id_pic,
		":login" => $login]);
	return true;
}

function comments_idpic($id_pic) {
	global $db;
	$query = $db->prepare("SELECT `id`, `id_login`, `text` FROM `comments`
		WHERE `id_pic` = :id_pic");
	$query->execute([":id_pic" => $id_pic]);
	return $query->fetchAll();
}

function comments_idpic_add($id_pic, $login, $text) {
	global $db;
	$query = $db->prepare("INSERT INTO `comments`
		VALUES(0, :id_pic, :login, :text)");
	$query->execute([
		":id_pic" => $id_pic,
		":login" => user_id_bylogin($login),
		":text" => $text]);
}

function comment_remove($id) {
	global $db;
	$query = $db->prepare("DELETE FROM `comments`
		WHERE `id` = :id");
	$query->execute([":id" => $id]);
}

function comment_author($id) {
	global $db;
	$query = $db->prepare("SELECT id_login FROM `comments`
		WHERE `id` = :id");
	$query->execute([":id" => $id]);
	$query->fetch()[0];
}

function notification_enabled_for($login) {
	global $db;
	$query = $db->prepare("SELECT `login` FROM `users`
		WHERE `login` = :login AND `notification` = 1");
	$query->execute([":login" => $login]);
	$result = $query->fetch();

	if (count($result) > 1)
		return 1;
	else
		return 0;
}

function user_change_notify($login, $value) {
	global $db;
	$query = $db->prepare("UPDATE `users` SET `notification` = :value
		WHERE `login` = :login");
	$query->execute([
		":value" => $value,
		":login" => $login]);
}

function notify_pic_author($idpic, $login, $comment) {
	$picauthor = pic_login_of($idpic);
	$picemail = user_get_email($picauthor)[0];

	$to      = $picemail;
	$subject = "Camagru - Nouveau commentaire!";
	$message = "L'utilisateur $login vient de commenter une de vos images!\r\n".
		"\"$comment\"\r\n".
		"http://". $_SERVER["HTTP_HOST"] ."/gallery.php";
	$headers = "From: echojnow@student.42.fr\r\n";

	mail($to, $subject, $message, $headers);
}
?>

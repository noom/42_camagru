<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"])) {
	echo("0");
	die();
}

if (notification_enabled_for($_SESSION["logged_on_user"]))
	echo("1");
else
	echo("0");

die();
?>

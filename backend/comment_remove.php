<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"])
	|| comment_author($_GET["id"]) != user_id_bylogin($_SESSION["logged_on_user"])) {
	header('Location: /gallery.php');
	die();
}

comment_remove(htmlspecialchars($_GET["id"]));

header('Location: /gallery.php');
die();
?>

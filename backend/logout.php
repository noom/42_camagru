<?php
session_start();

unset($_SESSION["logged_on_user"]);
header('Location: /index.php?log=out');
die();
?>

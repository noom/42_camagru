<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"])) {
	echo("error-user");
	die();
}

if (empty($_POST["id"])) {
	echo("error-pic");
	die();
}

like_pic_id($_SESSION["logged_on_user"], $_POST["id"]);

echo(json_encode(array(
	"likes" => count(pic_likes_of($_POST["id"])),
	"selected" => like_query($_SESSION["logged_on_user"], $_POST["id"]))));
?>

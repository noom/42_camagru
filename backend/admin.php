<?php
session_start();

include "bdd.php";

if (!user_is_admin($_SESSION["logged_on_user"]))
	die();

if ($_GET["section"] == "category") {
	if ($_GET["action"] == "ajouter") {
		if (empty($_GET["name"])) {
			header("Location: /admin_page.php?error=empty");
			die();
		}
		add_category($_GET["name"]);
	}
	else if ($_GET["action"] == "delete") {
		del_category($_GET["name"]);
	}
	else if ($_GET["action"] == "editer") {
		if (empty($_GET["newname"])) {
			header("Location: /admin_page.php?error=empty");
			die();
		}
		edit_category($_GET["oldname"], $_GET["newname"]);
	}
}

else if ($_GET["section"] == "product") {
	if ($_GET["action"] == "ajouter") {
		if (empty($_GET["name"]) || empty($_GET["fullname"])
			|| $_GET["price"] < 1 || empty($_GET["description"])) {
			header("Location: /admin_page.php?error=empty");
			die();
		}
		add_product($_GET["category"], $_GET["name"], $_GET["price"], $_GET["fullname"], $_GET["description"], "default");
	}
	else if ($_GET["action"] == "delete") {
		del_product($_GET["category"], $_GET["name"]);
	}
	else if ($_GET["action"] == "editer") {
		if (empty($_GET["newname"])) {
			header("Location: /admin_page.php?error=empty");
			die();
		}
		edit_product($_GET["category"], $_GET["oldname"], $_GET["newname"]);
	}
}

else if ($_GET["section"] == "user") {
	if ($_GET["action"] == "ajouter") {
		if (empty($_GET["name"]) || empty($_GET["passwd"])) {
			header("Location: /admin_page.php?error=empty");
			die();
		}
		add_user($_GET["name"], $_GET["passwd"], $_GET["group"]);
	}
	else if ($_GET["action"] == "delete") {
		del_user($_GET["name"]);
	}
	else if ($_GET["action"] == "editer") {
		edit_user_group($_GET["name"], $_GET["group"]);
	}
}

if (!empty($_SERVER["HTTP_REFERER"]))
	header("Location: ". $_SERVER["HTTP_REFERER"]);
else
	echo("Something bad happened... it hurts...");
die();
?>

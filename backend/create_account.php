<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_POST["submit"]) || empty($_POST["login"])
	|| empty($_POST["passwd"]) || empty($_POST["email"])) {
	header("Location: /create_account_page.php?error=empty");
	die();
}

$_POST["login"] = htmlspecialchars($_POST["login"]);
$_POST["passwd"] = htmlspecialchars($_POST["passwd"]);
$_POST["email"] = htmlspecialchars($_POST["email"]);

if (user_exists($_POST["login"])) {
	header("Location: /create_account_page.php?error=exists");
	die();
}

if (email_exists($_POST["email"])) {
	header("Location: /create_account_page.php?error=email_exists");
	die();
}

$email_filtered = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
if ($email_filtered === false) {
	header("Location: /create_account_page.php?error=invalid_email");
	die();
}

if (strlen($_POST["passwd"]) < 6) {
	header("Location: /create_account_page.php?error=weak_passwd");
	die();
}

user_add($_POST["login"], $_POST["passwd"], $_POST["email"]);
header("Location: /create_account_page.php?error=success");
die();
?>

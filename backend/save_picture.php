<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_SESSION["logged_on_user"])) {
	echo(json_encode(["error" => "error-pic"]));
	die();
}

if (empty($_POST["filter"])) {
	echo(json_encode(["error" => "error-pic"]));
	die();
}

if (empty($_POST["pic"])) {
	echo(json_encode(["error" => "error-pic"]));
	die();
}

$pic_base64 = explode(",", $_POST["pic"])[1];

if (strlen($pic_base64) > 4000000) {
	echo(json_encode(["error" => "error-size"]));
	die();
}

$pic_raw = base64_decode($pic_base64);
$pic = @imagecreatefromstring($pic_raw);

if (empty($pic)) {
	echo(json_encode(["error" => "error-format"]));
	die();
}

$pic_size = getimagesizefromstring($pic_raw);

if ($pic_size[0] != 560 || $pic_size[1] != 420) {
	$pic = imagescale($pic, 560, 420);
	$pic_size = [imagesx($pic), imagesy($pic)];
}

$filter_path = $_SERVER["DOCUMENT_ROOT"] ."/img/". $_POST["filter"] .".png";
$filter = imagecreatefrompng($filter_path);
$filter_size = getimagesize($filter_path);

imagecopy($pic, $filter, 0, 0, 0, 0, $pic_size[0], $pic_size[1]);

ob_start();
imagepng($pic);
$newpic = ob_get_clean();

$newpic_id = pic_save($_SESSION["logged_on_user"], $newpic);

$response = json_encode([
	"login" => $_SESSION["logged_on_user"],
	"id" => $newpic_id,
	"pic" => base64_encode($newpic)]);
echo($response);
?>

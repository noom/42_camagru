<?php
session_start();

include "bdd.php";

$item_exists = false;

if ($_GET["submit"] == "Ajouter au panier" && $_GET["quantity"] > 0) {
	if (!empty($_SESSION["cart"])) {
		foreach ($_SESSION["cart"] as &$product) {
			if ($product["category"] == $_GET["category"] && $product["name"] == $_GET["name"]) {
				$product["quantity"] += $_GET["quantity"];
				$item_exists = true;
			}
		}
	}
	else
		$_SESSION["cart"] = array();

	if (!$item_exists) {
		$_SESSION["cart"][] = array(
			"category" => $_GET["category"],
			"name" => $_GET["name"],
			"quantity" => $_GET["quantity"]
		);
	}
}
else if ($_GET["action"] == "remove") {
	unset($_SESSION["cart"]);
}
else if ($_GET["action"] == "validate") {
	if (empty($_SESSION["logged_on_user"])) {
		header("Location: /cart_page.php?error=log");
		die();
	}
	else {
		add_to_archive($_SESSION["cart"]);
		unset($_SESSION["cart"]);
		header("Location: /cart_page.php?error=success");
		die();
	}
}
else if ($_GET["action"] == "remove_one") {
	foreach ($_SESSION["cart"] as $key => &$product) {
		if ($product["category"] == $_GET["category"]
			&& $product["name"] == $_GET["name"]) {
			if ($product["quantity"] <= 1)
				unset($_SESSION["cart"][$key]);
			else
				$product["quantity"]--;
		}
	}
}

if (!empty($_SERVER["HTTP_REFERER"]))
	header("Location: ". $_SERVER["HTTP_REFERER"]);
else
	echo("Something bad happened... it hurts...");
die();
?>

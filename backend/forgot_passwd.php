<?php
session_start();

include $_SERVER["DOCUMENT_ROOT"] ."/backend/bdd.php";

if (empty($_POST["login"])) {
	header("Location: /login_page.php?log=forgot_passwd_empty");
	die();
}

$_POST["login"] = htmlspecialchars($_POST["login"]);

if (!user_exists($_POST["login"])) {
	header("Location: /login_page.php?log=forgot_passwd_unknown");
	die();
}

user_forgot_passwd($_POST["login"]);
header("Location: /login_page.php?log=forgot_passwd_success");
die();
?>

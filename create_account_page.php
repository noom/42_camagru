<?php
include "config/setup.php";
include "backend/bdd.php";
if (!empty($_SESSION["logged_on_user"])) {
	header("Location: /index.php");
	die();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Home</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<h1><a href="/index.php">camagru</a></h1>

		<div id="menu-container">
		<ul>
		<?php
			if (empty($_SESSION["logged_on_user"]))
				echo("<li><a href='/login_page.php'>Se connecter</a></li>");
			else {
				echo($_SESSION["logged_on_user"]);
				echo("<li><a href='/backend/logout.php'>Se deconnecter</a></li>");
			}
		?>
		<li><a href='/gallery.php'>C'est partis pour la gallerie</a></li>
		</ul>
		</div>

		<h2>Creer un compte</h2>

		<?php
			if ($_GET["error"] == "success")
				echo("<p class='notify-good'>Le compte a bien ete cree! Veuillez verifier vos mails afin d'activer votre compte.</p>");
			else if ($_GET["error"] == "empty")
				echo("<p class='notify-bad'>Vous devez renseigner tous les champs!</p>");
			else if ($_GET["error"] == "exists")
				echo("<p class='notify-bad'>Cette identifiant n'est pas disponible!</p>");
			else if ($_GET["error"] == "invalid_email")
				echo("<p class='notify-bad'>Cette adresse mail n'est pas valide!</p>");
			else if ($_GET["error"] == "email_exists")
				echo("<p class='notify-bad'>Cette adresse mail est deja utilise.</p>");
			else if ($_GET["error"] == "weak_passwd")
				echo("<p class='notify-bad'>Ce mot de passe est trop faible.</p>");
		?>

	<form action="/backend/create_account.php" method="post" class='myform'>
		Identifiant: <input type="text" name="login">
		<br />
		Mot de passe: <input type="password" name="passwd">
		<br />
		Adresse mail: <input type="text" name="email">
		<br />
		<input type="submit" name="submit" value="OK">
	</form>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

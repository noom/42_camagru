<?php include "config/setup.php"; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Camagru</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<h1><a href="/index.php">camagru</a></h1>

		<div id="menu-container">
		<ul>
		<?php
			if (empty($_SESSION["logged_on_user"])) {
				echo("<li><a href='/login_page.php'>Se connecter</a></li>");
				echo("<li><a href='/create_account_page.php'>Creer un compte</a></li>");
			}
			else {
				echo("<li><strong><a href='/modify_account.php'>". $_SESSION["logged_on_user"] ."</a></strong></li>");
				echo("<li><a href='/backend/logout.php'>Se deconnecter</a></li>");
				echo("<li><a href='/picture.php'>Allons prendre des p'tites photos!</a></li>");
			}
		?>
		</ul>
		</div>

		<h2>Voyons voyons . . .</h2>

		<?php
			include "backend/display_pics.php";
		?>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

<script>
	function likePic(ev) {
		var id = ev.parentNode.children["id"].getAttribute("value");

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/like_pic.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(eve) {
			if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				xhr_decoded = JSON.parse(xhr.response);
				ev.parentNode.children["likes_count"].innerHTML = xhr_decoded["likes"];
				if (xhr_decoded["selected"])
					ev.parentNode.children["thumb"].classList.add("selected");
				else
					ev.parentNode.children["thumb"].classList.remove("selected");
			}
		}
		xhr.send("id=" + encodeURIComponent(id));
	}
</script>

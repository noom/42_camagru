<?php include "config/setup.php"; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Login Page</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<h1><a href="/index.php">camagru</a></h1>

		<div id='menu-container'>
		<ul>
		<?php if (empty($_SESSION["logged_on_user"])) { ?>
			<li><a href='/create_account_page.php'>Creer un compte</a></li>
		<?php } ?>
			<li><a href='/gallery.php'>C'est partis pour la gallerie</a></li>
		</ul>
		</div>

		<h2>Se connecter</h2>
		<?php
			if ($_GET["log"] == "empty")
				echo("<p class='notify-bad'>Vous devez renseigner votre identifiant et votre mot de passe.</p>");
			else if ($_GET["log"] == "wrong")
				echo("<p class='notify-bad'>Le couple identifiant / mot de passe est incorrect ou votre compte n'est pas encore valide.</p>");

			else if ($_GET["log"] == "forgot_passwd_empty")
				echo("<p class='notify-bad'>Vous devez renseigner votre identifiant.</p>");
			else if ($_GET["log"] == "forgot_passwd_unknown")
				echo("<p class='notify-bad'>Identifiant inconnu.</p>");
			else if ($_GET["log"] == "forgot_passwd_success")
				echo("<p class='notify-good'>Un nouveau mot de passe vous a ete envoye par mail.</p>");

			else if ($_GET["log"] == "email_ko")
				echo("<p class='notify-bad'>Votre code de confirmation ne fonctionne pas!</p>");
			else if ($_GET["log"] == "email_ok")
				echo("<p class='notify-good'>Votre compte a bien ete valide!</p>");

			if (empty($_SESSION["logged_on_user"])) {
		?>
		<form action="/backend/login.php" method="post" class='myform'>
			Identifiant: <input type="text" name="login">
			<br />
			Mot de passe: <input type="password" name="passwd">
			<br />
			<input type="submit" name="submit" value="OK">
		</form>

		<br />

		<h2>Mot de passe oublie?</h2>
		<form action="/backend/forgot_passwd.php" method="post" class='myform'>
			Identifiant: <input type="text" name="login">
			<br />
			<input type="submit" name="submit" value="OK">
		</form>
		<?php
			}
			else {
				echo("<p class='notify-bad'>Vous etes deja connecte.</p>");
			}
		?>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

<?php
include "config/setup.php";
include "backend/bdd.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Camagru</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<h1><a href="/index.php">camagru</a></h1>

		<div id="menu-container">
		<ul>
		<?php
			if (empty($_SESSION["logged_on_user"])) {
				echo("<li><a href='/login_page.php'>Se connecter</a></li>");
				echo("<li><a href='/create_account_page.php'>Creer un compte</a></li>");
			}
			else {
				echo("<li><strong><a href='/modify_account.php'>". $_SESSION["logged_on_user"] ."</a></strong></li>");
				echo("<li><a href='/backend/logout.php'>Se deconnecter</a></li>");
				echo("<li><a href='/picture.php'>Allons prendre des p'tites photos!</a></li>");
			}
		?>
		<li><a href='/gallery.php'>C'est partis pour la gallerie</a></li>
		</ul>
		</div>

		<!-- <div id='menu'>
		<?php
			/* if (empty($_SESSION["logged_on_user"])) { */
			/* 	echo("<a href='/login_page.php'>Se connecter</a>"); */
			/* 	echo("<a href='/create_account_page.php'>Creer un compte</a>"); */
			/* } */
			/* else { */
			/* 	echo("<a href='/modify_account.php'>". $_SESSION["logged_on_user"] ."</a>"); */
			/* 	echo("<a href='/backend/logout.php'>Se deconnecter</a>"); */
			/* 	echo("<a href='/picture.php'>Allons prendre des p'tites photos!</a>"); */
			/* } */
		?>
			<a href='/gallery.php'>C'est partis pour la gallerie</a>
		</div> -->

		<?php
			if ($_GET["log"] == "in") {
				echo("<p class='notify-good'>Vous etes bien connecte.</p>");
			}
			else if ($_GET["log"] == "out") {
				echo("<p class='notify-good'>Vous etes bien deconnecte.</p>");
			}
		?>

		<h2>Bienvenue sur CAMAGRU!</h2>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

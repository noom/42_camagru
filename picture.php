<?php
include "config/setup.php";
include "backend/bdd.php";
if (empty($_SESSION["logged_on_user"])) {
	header("Location: /index.php");
	die();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Camagru</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<h1><a href="/index.php">camagru</a></h1>

		<div id="menu-container">
		<ul>
			<li><a href="/modify_account.php"><strong><?php echo($_SESSION["logged_on_user"]) ?></strong></a></li>
			<li><a href="/backend/logout.php">Se deconnecter</a></li>
			<li><a href="/gallery.php">C'est partis pour la gallerie</a></li>
		</ul>
		</div>

		<h2>Prends des photos super chouettes!</h2>

		<div id="big-container">
			<div id="camera-container">
				<canvas id="camera-canvas" style='display: none'>
				</canvas>
				<img id="filter-current" src="/img/rope.png" class="camera-over">
				<div class="camera-view" class="camera-under">
					<video id="video" style="display: none"></video>
					<img id="localpic-preview" ></img>
				</div>
				<div id="filters-container">
					<img class="filter selected" id="rope" src="/img/rope.png" onclick="chooseFilter(this)">
					<img class="filter" id="knife" src="/img/knife.png" onclick="chooseFilter(this)">
					<img class="filter" id="blood_frame" src="/img/blood_frame.png" onclick="chooseFilter(this)">
				</div>
				<button id="startbutton">Prends la photo!</button>
				<input id="localpic" type="file" accept="image/*">
				<button id="localpicbutton">Envoyer la photo</button>
			</div>
			<div id="pics-container">
			<?php
				$pics = array_reverse(pic_by($_SESSION["logged_on_user"]));
				foreach ($pics as $pic) {
					echo("<div class='pic-preview' onclick='deletePic(this)'>".
						"<div style='display: none' name='id' value='". $pic["id"] ."'></div>".
						"<img src='data:image/png;base64,". $pic["pic_b64"] ."'>".
						"</div>");
				}
			?>
			</div>
		</div>
	</body>

	<footer>
		<hr>
		<p id="copyright">Eyal Chojnowski © copyright 2018</p>
	</footer>
</html>

<script>
	clean(document);

	var streaming = false,
		video = document.querySelector("#video"),
		canvas = document.querySelector("#camera-canvas"),
		localpic = document.querySelector("#localpic"),
		localpic_preview = document.querySelector("#localpic-preview"),
		startbutton = document.querySelector("#startbutton"),
		localpicbutton = document.querySelector("#localpicbutton"),
		video_width = 560,
		video_height = 420, // this is updated according to width
		canvas_width = video_width,
		canvas_height = video_height;
		selected_filter = "rope";

	if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
			video.srcObject = stream;
			video.play();
		});
	}

	video.addEventListener("canplay", function(ev) {
		if (!streaming) {
			video_height = video.videoHeight / (video.videoWidth / video_width);
			video.setAttribute("width", video_width);
			video.setAttribute("height", video_height);
			canvas.setAttribute("width", canvas_width);
			canvas.setAttribute("height", canvas_height);
			streaming = true;
		}
	}, false);

	startbutton.addEventListener("click", function(ev) {
		takePicture();
		ev.preventDefault();
	}, false);

	localpicbutton.addEventListener("click", function(ev) {
		takePictureLocal();
		ev.preventDefault();
	}, false);

	localpic.addEventListener("change", function(ev) {
		localpic_preview.src = window.URL.createObjectURL(localpic.files[0]);
		/* img.onload = function() { */
		/* 	window.URL.revokeObjectURL(this.src); */
		/* } */
	}, false);


	function chooseFilter(ev) {
		var container = document.getElementById("filters-container");
		for (var child = container.firstChild; child !== null; child = child.nextSibling) {
			child.classList.remove("selected");
		}
		ev.classList.add("selected");
		selected_filter = ev.id;

		var current_filter = document.getElementById("filter-current");
		current_filter.setAttribute("src", ev.src);
	}

	function deletePic(ev) {
		var id = ev.children["id"].getAttribute("value");

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/delete_picture.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(eve) {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				ev.parentNode.removeChild(ev);
			}
		}
		xhr.send("id=" + encodeURIComponent(id));
	}

	function takePicture() {
		canvas.width = canvas_width;
		canvas.height = canvas_height;
		canvas.getContext("2d").drawImage(video, 0, 0, canvas_width, canvas_height);
		var pic_data = canvas.toDataURL("image/png");

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/save_picture.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(ev) {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				var json_response = JSON.parse(xhr.response);
				var pics_container = document.getElementById("pics-container");

				var newpic = document.createElement("div");
				newpic.setAttribute("class", "pic-preview");
				newpic.setAttribute("onclick", "deletePic(this)");

				var login = document.createElement("div");
				login.setAttribute("name", "login");
				login.setAttribute("value", json_response["login"]);
				login.setAttribute("style", "display: none");

				var id = document.createElement("div");
				id.setAttribute("name", "id");
				id.setAttribute("value", json_response["id"]);
				id.setAttribute("style", "display: none");

				var img = document.createElement("img");
				img.setAttribute("src", "data:image/png;base64," + json_response["pic"]);

				newpic.appendChild(login);
				newpic.appendChild(id);
				newpic.appendChild(img);

				pics_container.insertBefore(newpic, pics_container.firstChild);
			}
		}
		xhr.send("pic=" + encodeURIComponent(pic_data) +
			"&filter=" + encodeURIComponent(selected_filter));
	}

	function FileUpload(img, file) {
		var reader = new FileReader();
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/save_picture.php");

		xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');

		reader.onload = function(evt) {
			xhr.send(evt.target.result);
		};
		reader.readAsDataURL(localpic_preview.src);
	}

	function takePictureLocal() {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/backend/save_picture.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onreadystatechange = function(ev) {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				var json_response = JSON.parse(xhr.response);

				if (json_response.hasOwnProperty("error")) {
					return ;
				}

				var pics_container = document.getElementById("pics-container");

				var newpic = document.createElement("div");
				newpic.setAttribute("class", "pic-preview");
				newpic.setAttribute("onclick", "deletePic(this)");

				var login = document.createElement("div");
				login.setAttribute("name", "login");
				login.setAttribute("value", json_response["login"]);
				login.setAttribute("style", "display: none");

				var id = document.createElement("div");
				id.setAttribute("name", "id");
				id.setAttribute("value", json_response["id"]);
				id.setAttribute("style", "display: none");

				var img = document.createElement("img");
				img.setAttribute("src", "data:image/png;base64," + json_response["pic"]);

				newpic.appendChild(login);
				newpic.appendChild(id);
				newpic.appendChild(img);

				pics_container.insertBefore(newpic, pics_container.firstChild);
			}
		}

		var reader = new FileReader();
		reader.onload = function(ev) {
			xhr.send("pic=" + encodeURIComponent(ev.target.result) +
				"&filter=" + encodeURIComponent(selected_filter));
		}
		reader.readAsDataURL(localpic.files[0]);
	}

	function clean(node) {
		for (var n = 0; n < node.childNodes.length; n++) {
			var child = node.childNodes[n];
			if (child.nodeType === 8
				|| (child.nodeType === 3 && !/\S/.test(child.nodeValue))) {
				node.removeChild(child);
				n--;
			}
			else if(child.nodeType === 1)
				clean(child);
		}
	}
</script>
